package com.lab8.week8;

public class TestTree {
    private String Tree;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public TestTree(String Tree,char symbol,int x,int y){
        this.Tree = Tree;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }


    public TestTree(String Tree, char symbol){ //signature
        this(Tree , symbol, 0 , 0);
    }


    public void print(){
        System.out.println(Tree + "X: " + x + "y: " + y);
    }
    public boolean up(){
        if(y == MIN_Y) return false;
        this.y = this.y - 1;

        return true;
    }

    public boolean up(int step){
        for(int i=0; i<step; i++){
            if(!up()){
                return false;
            }    
        }
        return true;
    }

 
    
   

    public boolean left(){
        x = x - 1;
        return true;
    }

    public boolean left(int step){
        for(int i=0; i<step; i++){
            if(!left()){
                return false;
            }    
        }
        return true;
    }

    public boolean right(){
        x = x + 1;
        return true;
    }

    public boolean right(int step){
        for(int i=0; i<step; i++){
            if(!right()){
                return false;
            }    
        }
        return true;
    }
   
    public void setTree(String Tree){
        this.Tree = Tree;
    }
    public String getTree(){  //Getter methods
        return Tree;
    }
    public char getsymbol(){
        return symbol;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public boolean ganX(){
        if (x <= MAX_X && x >= MIN_X)return true;
        return false;
    }
    public boolean ganY(){
        if (y <= MAX_Y && y >= MIN_Y)return true;
        return false;
    }

}
