package com.lab8.week8;

public class RectangleShape {
    private int width;
    private int height;

    public RectangleShape(int width, int height) {
        this.width = width;
        this.height = height;

    }


    public int findArea() {
        int area = width * height;
        System.out.println("Find Area of Rectangle");
        System.out.println("Width : " + width);
        System.out.println("Height : " + height);
        System.out.println("Area : " + area);
        System.out.println("----------------------------");
        return area;
    }


    public int findPerimeter() {
        int perimeter = (width + height) * 2;
        System.out.println("Find Perimeter of Rectangle");
        System.out.println("Width : " + width);
        System.out.println("Height : " + height);
        System.out.println("Perimeter : " + perimeter);
        System.out.println("----------------------------");
        return perimeter;

    }
}