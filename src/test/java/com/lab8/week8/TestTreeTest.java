package com.lab8.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTreeTest {
    @Test
    public void shouldCreateTestTreeSuccess1(){
        TestTree TestTree1 = new TestTree("TestTree1", 'F',5,10);
        assertEquals("TestTree1", TestTree1.getTree());
        assertEquals('F', TestTree1.getsymbol());
        assertEquals(5, TestTree1.getX());
        assertEquals(10, TestTree1.getY());
    }
    @Test
    public void shouldCreateTestTreeSuccess2(){
        TestTree TestTree2 = new TestTree("TestTree2", 'S',5,11);
        assertEquals("TestTree2", TestTree2.getTree());
        assertEquals('S', TestTree2.getsymbol());
        assertEquals(5, TestTree2.getX());
        assertEquals(11, TestTree2.getY());
    }

    @Test
    public void shouldCreateTestTreeOver1(){
        TestTree TestTree2 = new TestTree("TestTree2", 'S',20,40);
        assertEquals("TestTree2", TestTree2.getTree());
        assertEquals('S', TestTree2.getsymbol());
        assertEquals(false, TestTree2.ganX());
        assertEquals(11, TestTree2.getY());
    }

    @Test
    public void shouldCreateTestTreeOver2(){
        TestTree TestTree2 = new TestTree("TestTree2", 'S',5,60);
        assertEquals("TestTree2", TestTree2.getTree());
        assertEquals('S', TestTree2.getsymbol());
        assertEquals(false, TestTree2.ganY());
        assertEquals(5, TestTree2.getX());
    }
}









